/*
 *	Main Config
 */

 module.exports = {
 	port: process.env.PORT || 8000,
 	views: 'jade'
 }