/*
 * Routes
 */

module.exports = {
	'/': 'MainController.index',
	'/info': 'MainController.info',


	'POST /user': 'api.create',
	'GET /user': 'api.find',
	'DELETE /user': 'api.delete',

	'POST /message': 'api.message_create',
	'GET /message': 'api.message_find',
	'DELETE /message': 'api.message_delete',

	'GET /message-load': 'api.message_load',

	'POST /channel': 'api.channel_create',
	'GET /channel': 'api.channel_find',
	'DELETE /channel': 'api.channel_delete',
}