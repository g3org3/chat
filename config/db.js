/*
 *	DB.js
 */

module.exports = {
	/*
	mongo: {
		database: 'test',
		port: 27017,
		host: 'localhost'
	}
	*/

	rethinkdb: {
		port: 28015,
		host: 'localhost'
	},

	database: 'chat'
}