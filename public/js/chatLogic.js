'use strict';

var socket = io.connect();
var state = {blur: false, focus: true};
var _name = "GUEST";
var _unread = 0;

if(prompt("code")!="2914")
	document.location.href="http://"

angular.module('chat', ['ngSanitize'])
.controller('chatController', function($scope, $http, $sce, $timeout){
	
	/**
	 *	Vars
	 */
	$scope.messages = [];
	$scope.newmessage = {name: "", body: ""};
	$scope.user = {id: -1, name: "Guest"}
	$scope.users = [];

	/**
	 *	METHODS
	 */

	$scope.load = function(){
		$http.post('/message-load', {total: $scope.messages.length}).then(function(response){
			$scope.messages = response.data;
		})
	}

	$scope.add = function(){
		$scope.messages = _.map($scope.messages, function(message){
			if(message.new)
				message.out = true;
			return message;
		})
		$timeout(function(){
			$scope.messages = _.filter($scope.messages, function(message){
				return message.new==undefined;
			})
		}, 1000)
		if($scope.newmessage.body=="") return;
		$scope.newmessage.name = _name;
		//- $scope.messages.push({name:$scope.newmessage.name, body:$scope.newmessage.body});
		$http.post('/message', {message: $scope.newmessage}).then(function(response){
			//- $scope.messages = response.data;
			console.log(response)
			$scope.newmessage.body="";
			
		})
	}
	$scope.checkUrl = function(message){
		return message.body.indexOf('http://')==0 || message.body.indexOf('https://')==0
	}
	$scope.checkImg = function(message){
		if(message.body.indexOf('http://')==0 || message.body.indexOf('https://')==0){
			return message.body.indexOf('.png')!=-1 || message.body.indexOf('.jpg')!=-1
		} else {
			return false;
		}
	}
	$scope.checkVid = function(message){
		return message.body.indexOf('www.youtube.com')!=-1 || message.body.indexOf('https://youtu.be')==0
	}
	$scope.checkMp4 = function(message){
		return message.body.indexOf('.mp4')!=-1
	}
	$scope.getVideo = function(message){
		if(!$scope.checkUrl(message) || !$scope.checkMp4(message)) return "";
		var html = "<div data-mp4=\""+message.body+"\">"+
			"<video preload=\"auto\" style=\"min-height:280.4347826087px;width: 500px;\" width=\"500\" loop=\"\" muted=\"\" autoplay=\"\">"+
			"<source src=\""+message.body+"\" type=\"video/mp4\">"+
			"</video>"+
		"</div>";
		return $sce.trustAsHtml(html);
	}
	$scope.getCodeImg = function(message){
		if(!$scope.checkUrl(message) || !$scope.checkImg(message)) return "";
		return $sce.trustAsHtml("<img src=\""+message.body+"\" />");
	}
	$scope.getCode = function(message){
		var url = message.body;
		var na = false;

		if(url.indexOf('https://youtu.be')==0){
			url = url.substr(("https://youtu.be/").length);
		} else if(url.indexOf('www.youtube.com')!=-1){
			url = url.substr(url.indexOf("v=")+2);
			if(url.indexOf("&")!=-1){
				url.substr(0,url.indexOf("&"))
			}
		} else {
			return "";
		}

		var html = "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/"+
					url+"\" frameborder=\"0\" allowfullscreen></iframe>";
		return $sce.trustAsHtml(html);
	}

	/**
	 * Socket EVENTS
	 */
	socket.on('welcome', function(users){
		var value = prompt("your name?");
		_name = (value)? value: _name;
		$scope.user.name = _name;
		$scope.user.id = users.length;

		// notify all users
		socket.emit('to.server.new.user', _name);
		
		// get messages
		$http.get('/message').then(function(response){
			$scope.users = users;
			$scope.messages = response.data;
			setTimeout(function(){
				var div = document.getElementById('messages');
				div.scrollTop = div.scrollHeight;
			}, 50);
		})
	})

	socket.on('to.client.new.user', function(user){
		$scope.users.push(user);
		$scope.$apply();
	})

	socket.on('update', function(item){
		if(item.new_val!=null && item.old_val==null){
			console.log("creation")
			
			if(_unread==0 && state.blur)
				$scope.messages.push({new: true, body: ""});
			$scope.messages.push(item.new_val);
			setTimeout(function(){
				var div = document.getElementById('messages');
				div.scrollTop = div.scrollHeight;
				if(state.blur){
					_unread++;
					document.title = "("+_unread+") new message";
					var v = document.getElementsByTagName("audio")[0]
					v.play();
				}
			}, 50);
			$scope.$apply();
		} else if(item.new_val==null && item.old_val!=null){
			console.log("deletetion");
			new_data = []
			angular.forEach($scope.messages, function(user){
				if(user.id!=item.old_val.id)
					new_data.push(user);
			})
			$scope.messages = new_data;
			$scope.$apply();
		} else {
			console.log(item);
			console.log("deberia ser un update");
		}
	})

	socket.on('to.client.user.update', function(user){
		var pos = -1;
		for (var i = 0; i < $scope.users.length; i++) {
			if($scope.users[i].name==user.name){
				pos = i;
				break;
			}
		};
		$scope.users[pos] = user;
		$scope.$apply();
	})

	window.onblur=function(){
		state.blur = true;
		state.focus = false;
		$scope.user.status = 'idle';
		socket.emit('to.server.user.update', $scope.user)
	}

	window.onfocus=function(){
		$scope.user.status = 'online';
		socket.emit('to.server.user.update', $scope.user)
		state.blur = false;
		state.focus = true;
		document.title="Chat";


		_unread = 0;
	}
})