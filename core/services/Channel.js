/**
 * Messages Model
 */

module.exports = (function(){
	// Dependencies
	var r 		= require('rethinkdb')
	var debug 	= require('debug')
	var Q 		= require('q');

	// Private Vars
	var _options = webjs.db.rethinkdb;
	var _db = webjs.db.database;
	var _table = "channels";

	// Public Methods
	var model = {
		create: function(model){
			var defer = Q.defer();

			r.connect(_options).then(function(conn){
				model.createdAt = r.now();
				model.updatedAt = r.now();

				return r.db(_db).table(_table).insert(model).run(conn)
					.finally(function(){ conn.close(); });
			}).then(function(output){
				defer.resolve(output);
			}).error(function(err){
				defer.reject(err);
			})

			return defer.promise;
		},

		find: function(query){
			var defer = Q.defer();

			r.connect(_options).then(function(conn){
				return r.db(_db).table(_table).orderBy(r.desc("createdAt")).limit(30).orderBy("createdAt").run(conn)
					.finally(function(){ conn.close(); });

			}).then(function(cursor){
				return cursor.toArray();
			}).then(function(users){
				defer.resolve(users);
			}).error(function(err){
				defer.reject(err);
			})
			
			return defer.promise;
		},

		load: function(query){
			var defer = Q.defer();
			var limit = 30+query;
			r.connect(_options).then(function(conn){
				return r.db(_db).table(_table).orderBy(r.desc("createdAt")).limit(limit).orderBy("createdAt").run(conn)
					.finally(function(){ conn.close(); });
			}).then(function(cursor){
				return cursor.toArray();
			}).then(function(users){
				defer.resolve(users);
			}).error(function(err){
				defer.reject(err);
			})
			
			return defer.promise;
		},

		update: function(object){
			var defer = Q.defer();

			r.connect(_options).then(function(conn){
				object.updatedAt = r.now();

				return r.db(_db).table(_table).get(object.id).run(conn)
					.finally(function(){ conn.close(); });
			}).then(function(output){
				defer.resolve(output);
			}).error(function(err){
				defer.reject(err);
			})

			return defer.promise;
		},

		delete: function(id){
			var defer = Q.defer();

			r.connect(_options).then(function(conn){
				return r.db(_db).table(_table).get(id).delete().run(conn)
					.finally(function(){ conn.close(); });
			}).then(function(output){
				defer.resolve(output);
			}).error(function(err){
				defer.reject(err);
			})

			return defer.promise;
		}
	}

	return model;
}());