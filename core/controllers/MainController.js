/**
 * Main Controller
 */
var r = require('rethinkdb')

module.exports = {
	index: function(req, res) {
		res.render('index', {title: "Chat"})
	},

	info: function(req, res){
		var options = {
			host: 'localhost',
			port: 28015
		};

		r.connect(options, function(err, conn){
			if(err) console.error(err);
	
			r.db('shop').tableList().run(conn, function(err, cursor){
				if(err) console.error(err);
				console.log(cursor);
				conn.close(onCloseConnection);
				res.json(cursor);
			})
		});
	}
}

function onCloseConnection(err){
	if(err)
		throw err;
}

function onConnected(err, conn){
	if(err) console.error(err);
	
	r.db('shop').tableList().run(conn, function(err, cursor){
		if(err) console.error(err);
		console.log(cursor);
		conn.close(onCloseConnection);
	})
}