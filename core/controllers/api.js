/*
 * Api
 */

module.exports = {
	create: function(req, res){
		var user = req.param('user');

		if(!user) return res.json({
			message: "Error", 
			type: typeof user,
			recieved: user || "no params"
		});

		user.age = parseInt(user.age);

		User.create(user).then(function(result){
			res.json(result);
		}).fail(function(err){
			res.status(500).json({err: err});
		})
	},

	find: function(req, res){
		User.find().then(function(result){
			res.send(result)
		}).fail(function(err){
			res.status(500).json({err: err});
		})
	},

	delete: function(req, res){
		var id = req.param('id');

		if(!id) return res.json("Error");
		
		User.delete(id).then(function(){
			res.send("ok");
		}).fail(function(err){
			res.status(500).json({err: err});
		})

	},

	message_create: function(req, res){
		var message = req.param('message');

		var body = req.param('body');
		var name = req.param('name');
		var channelKey = req.param('channelKey');
		
		if(body && name && !message){
			message = {
				name: name,
				body: body,
				channelKey: channelKey
			};
		}

		if(!message) return res.json({
			message: "Error", 
			type: typeof message,
			recieved: message || "no params"
		});

		Message.create(message).then(function(result){
			res.json(result);
		}).fail(function(err){
			res.status(500).json({err: err});
		})
	},

	message_find: function(req, res){
		var id = req.param('channel');
		Message.find(id).then(function(result){
			res.send(result)
		}).fail(function(err){
			res.status(500).json({err: err});
		})
	},

	message_delete: function(req, res){
		var id = req.param('id');

		if(!id) return res.json("Error");
		
		Message.delete(id).then(function(){
			res.send("ok");
		}).fail(function(err){
			res.status(500).json({err: err});
		})

	},

	message_load: function(req, res){
		var total = req.param("total");
		var id = req.param('channel');
		if(total)
			total = parseInt(total);
		
		Message.load(id, total).then(function(result){
			res.send(result)
		}).fail(function(err){
			res.status(500).json({err: err});
		})
	},



	channel_create: function(req, res){
		var channel = req.param('channel');

		var name = req.param('name');
		if(name && !channel){
			channel = {name: name};
		}

		if(!channel) return res.json({
			channel: "Error", 
			type: typeof channel,
			recieved: channel || "no params"
		});

		Channel.create(channel).then(function(result){
			res.json(result);
		}).fail(function(err){
			res.status(500).json({err: err});
		})
	},

	channel_find: function(req, res){
		Channel.find().then(function(result){
			res.send(result)
		}).fail(function(err){
			res.status(500).json({err: err});
		})
	},

	channel_delete: function(req, res){
		var id = req.param('id');

		if(!id) return res.json("Error");
		
		Channel.delete(id).then(function(){
			res.send("ok");
		}).fail(function(err){
			res.status(500).json({err: err});
		})

	},

	channel_load: function(req, res){
		var total = req.param("total");

		Channel.load(total).then(function(result){
			res.send(result)
		}).fail(function(err){
			res.status(500).json({err: err});
		})
	}
}